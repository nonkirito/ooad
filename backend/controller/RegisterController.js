const Register = require('../models/Register')
const registerController = {
  /* data = [
         1
        {
            "_id" : ObjectId("60321bd9e5c1cb679b83451b"),
            "name" : "พีระพัฒน์",
            "lastname" : "ใจดี",
            "birthdate" : "23/09/2002",
            "gender" : "ชาย",
            "enlist" : "ผ่านเกณฑ์ทหาร",
            "email" : "pp@gmail.com",
            "password" : "pp123+",
            "confirmpassword" : "pp123+"
        },

         2
        {
            "_id" : ObjectId("603227f955931a3f04ffb2df"),
            "name" : "นานา",
            "lastname" : "นานิ๊",
            "birthdate" : "25/12/2000",
            "gender" : "หญิง",
            "enlist" : "ไม่ผ่านเกณฑ์ทหาร",
            "email" : "nana@gmail.com",
            "password" : "nn123+",
            "confirmpassword" : "nn123+",
            "__v" : 0
        },

         3
        {
            "_id" : ObjectId("60322bbb55931a3f04ffb2e1"),
            "name" : "สมชาย",
            "lastname" : "ใจเย็น",
            "birthdate" : "1990-02-14",
            "gender" : "ชาย",
            "enlist" : "ผ่านการเกณฑ์ทหาร",
            "email" : "hihi@gmail.com",
            "password" : "้ร้ร123",
            "confirmpassword" : "้ร้ร123",
            "__v" : 0
        }
    ] */
  async addRegister (req, res) {
    const payload = req.body
    console.log(payload)
    const register = new Register(payload)
    try {
      const news = await register.save()
      res.json(news)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getRegister (req, res) {
    try {
      const register = await Register.find({})
      res.json(register)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getRegisterOne (req, res) {
    const { id } = req.params
    try {
      const register = await Register.findById(id)
      res.json(register)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}
module.exports = registerController
