const express = require('express')
const router = express.Router()
const LoginComController = require('../controller/LoginCompanyService')

router.post('/register', LoginComController.registerNewUser)
router.post('/login', LoginComController.loginUser)

module.exports = router
