const Resume = require('../models/Resume')
const resumeController = {
  /* exampleData: [
    {
      id: 1,
      name: 'วิชัย',
      age: '20',
      gender: 'ชาย',
      phone: '0812345678',
      email: 'wichai@example.com',
      educationDegree: 'ปริญาตรี',
      skill: '#C Java',
      expextedSalary: '20,000-30,000',
      experience: 'เขียนภาษาได้ทุกภาษา'
    },
    {
      id: 2,
      name: 'นนธวัช',
      age: '20',
      gender: 'ชาย',
      phone: '0812345679',
      email: 'nonthawat@example.com',
      educationDegree: 'ปริญาตรี',
      skill: '#C Java R Python',
      expextedSalary: '20,000-30,000',
      experience: 'เขียนภาษาได้หลายภาษา'
    }
  ],
  lastId: 3, */
  async addResume (req, res) {
    const payload = req.body
    console.log(payload)
    const resume = new Resume(payload)
    try {
      const news = await resume.save()
      res.json(news)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async updateResume (req, res) {
    const payload = req.body
    try {
      const resume = await Resume.updateOne({ _id: payload._id }, payload)
      res.json(resume)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async deleteResume (req, res) {
    const { id } = req.params
    try {
      const resume = await Resume.deleteOne({ _id: id })
      res.json(resume)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getResume (req, res) {
    try {
      const resume = await Resume.find({})
      res.json(resume)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getResumeOne (req, res) {
    const { id } = req.params
    try {
      const resume = await Resume.findById(id)
      res.json(resume)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}
module.exports = resumeController
