const userService = {
  exampleData: [
    {
      id: 1,
      name: 'วิชัย',
      username: 'Wichai',
      password: '123Wichai@',
      confirmpassword: '123Wichai@',
      type: 'สมาชิก'
    },
    {
      id: 2,
      name: 'นนธวัช',
      username: 'Kirito',
      password: '123Kirito@',
      confirmpassword: '123Kirito@',
      type: 'เจ้าหน้าที่'
    },
    {
      id: 3,
      name: 'คลิกแบ็ค',
      username: 'clickback',
      password: '123clickback@',
      confirmpassword: '123clickback@',
      type: 'บริษัท'
    }
  ],
  lastId: 4,
  addUser (user) {
    user.id = this.lastId++
    this.exampleData.push(user)
    return user
  },
  updateUser (user) {
    const index = this.exampleData.findIndex(item => item.id === user.id)
    this.exampleData.splice(index, 1, user)
    return user
  },
  deleteUser (id) {
    const index = this.exampleData.findIndex(item => item.id === parseInt(id))
    this.exampleData.splice(index, 1)
    return { id }
  },
  getUsers () {
    return [...this.exampleData]
  },
  getUser (id) {
    const user = this.exampleData.find(item => item.id === parseInt(id))
    return user
  }
}

module.exports = userService
