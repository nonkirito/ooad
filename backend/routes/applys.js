const express = require('express')
const router = express.Router()
const applysController = require('../controller/ApplysController')

/* GET positions listing. */
router.get('/', applysController.getApplys)

router.get('/:id', applysController.getApply)

router.post('/', applysController.addApply)

router.put('/', applysController.updateApply)

module.exports = router
