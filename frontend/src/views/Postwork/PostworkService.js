const postworkService = {
  exampleData: [
    {
      id: 1,
      position: 'โปรแกรมเมอร์',
      salary: '25000',
      workplace: 'ตึก คลิกแบ็ค',
      province: 'ชลบุรี',
      numberofapplicants: 3,
      condition: 'จะต้องสามารถเขียนC++ และ Javaได้ มีประสบการณ์2ปี',
      welfare: 'สามารถลาหยุดได้ 7วัน/1ปี',
      opendate: '1/1/2564',
      closedate: '30/1/2564'
    }
  ],
  lastId: 2,
  addData (postwork) {
    postwork.id = this.lastId++
    this.exampleData.push(postwork)
  },
  updateData (postwork) {
    const index = this.exampleData.findIndex(item => item.id === postwork.id)
    this.exampleData.splice(index, 1, postwork)
  },
  deleteData (postwork) {
    const index = this.exampleData.findIndex(item => item.id === postwork.id)
    this.exampleData.splice(index, 1)
  },
  getPostworks () {
    return [...this.exampleData]
  }
}

export default postworkService
