const Post = require('../models/PostWork')
const searchController = {
  async getPostworkByPosition (req, res, next) {
    const { position } = req.params
    try {
      const post = await Post.find({ position: position }, function (
        err,
        docs
      ) {
        if (err) {
          console.log(err)
        } else {
          console.log('First function call : ', docs)
        }
      })
      res.json(post)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getPostworks (req, res, next) {
    try {
      const posts = await Post.find({})
      res.json(posts)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getPostwork (req, res, next) {
    const { id } = req.params
    try {
      const post = await Post.findById(id)
      res.json(post)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}

module.exports = searchController
