const express = require('express')
const cors = require('cors')
const path = require('path')
const cookieParser = require('cookie-parser')
const logger = require('morgan')
const mongoose = require('mongoose')

const indexRouter = require('./routes/index')
const usersRouter = require('./routes/users')
const positionsRouter = require('./routes/positions')
const resumesRouter = require('./routes/resumes')
const registerRouter = require('./routes/register')
const postRouter = require('./routes/postworks')
const searchRouter = require('./routes/search')
const applyRouter = require('./routes/applys')
const loginUserRouter = require('./routes/login')
const logincompanyRouter = require('./routes/loginCompany')

/* const app = express()
mongoose.connect('mongodb://localhost/myDB', {
  useNewUrlParser: true,
  useUnifiedTopology: true
}) */

const app = express()
mongoose.connect('mongodb://localhost/myDB', {
  useNewUrlParser: true,
  useUnifiedTopology: true
})

app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))
app.use(cors())

app.use('/', indexRouter)
app.use('/users', usersRouter)
app.use('/positions', positionsRouter)
app.use('/resumes', resumesRouter)
app.use('/register', registerRouter)
app.use('/postworks', postRouter)
app.use('/search', searchRouter)
app.use('/applys', applyRouter)
app.use('/setting', loginUserRouter)
app.use('/companysetting', logincompanyRouter)

module.exports = app
