const resumeService = {
  exampleData: [
    {
      id: 1,
      name: 'วิชัย',
      age: '20',
      gender: 'ชาย',
      phone: '0812345678',
      email: 'wichai@example.com',
      educationDegree: 'ปริญาตรี',
      skill: '#C Java',
      expextedSalary: '20,000-30,000',
      experience: 'เขียนภาษาได้ทุกภาษา'
    },
    {
      id: 2,
      name: 'นนธวัช',
      age: '20',
      gender: 'ชาย',
      phone: '0812345679',
      email: 'nonthawat@example.com',
      educationDegree: 'ปริญาตรี',
      skill: '#C Java R Python',
      expextedSalary: '20,000-30,000',
      experience: 'เขียนภาษาได้หลายภาษา'
    }
  ],
  lastId: 3,
  addResume (resume) {
    resume.id = this.lastId++
    this.exampleData.push(resume)
  },
  updateResume (resume) {
    const index = this.exampleData.findIndex(item => item.id === resume.id)
    this.exampleData.splice(index, 1, resume)
  },
  deleteResume (resume) {
    const index = this.exampleData.findIndex(item => item.id === resume.id)
    this.exampleData.splice(index, 1)
  },
  getResume () {
    return [...this.exampleData]
  }
}
export default resumeService
