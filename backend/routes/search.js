const express = require('express')
const router = express.Router()
const searchController = require('../controller/SearchController')

router.get('/', searchController.getPostworks)

router.get('/:position', searchController.getPostworkByPosition)

router.get('/:id', searchController.getPostwork)

module.exports = router
