const mongoose = require('mongoose')

const resumeSchema = new mongoose.Schema({
  name: String,
  age: String,
  gender: String,
  phone: String,
  email: String,
  educationDegree: String,
  skill: String,
  expextedSalary: String,
  experience: String
})

module.exports = mongoose.model('Resume', resumeSchema)
