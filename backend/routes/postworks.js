const express = require('express')
const router = express.Router()
const postsController = require('../controller/PostWorkController')

/* GET users listing. */
router.get('/', postsController.getPostworks)

router.get('/:id', postsController.getPostwork)

router.post('/', postsController.addPost)

router.put('/', postsController.updatePost)

router.delete('/:id', postsController.deletePost)

module.exports = router
