const express = require('express')
const router = express.Router()
const registerController = require('../controller/RegisterController')

router.get('/', registerController.getRegister)

router.get('/:id', registerController.getRegisterOne)

router.post('/', registerController.addRegister)

module.exports = router
