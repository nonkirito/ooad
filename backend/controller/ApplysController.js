const Apply = require('../models/Apply')
const applyController = {
  applyList: [
    {
      id: 1,
      name: 'วิชัย',
      surname: 'เสียงเลิศ',
      tel: '0812345678',
      email: 'wichai.example.com',
      resumeId: '602910fb677c64fbcd97cc43',
      experience: 2,
      expectedSalary: 20000,
      status: 'รอการพิจารณา',
      interviewDate: '03/03/2021'
    }
  ],
  lastId: 2,
  async addApply (req, res, next) {
    const payload = req.body
    console.log(payload)
    const apply = new Apply(payload)
    try {
      const newapply = await apply.save()
      res.json(newapply)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async updateApply (req, res, next) {
    const payload = req.body
    try {
      console.log(payload)
      const apply = await Apply.updateOne({ _id: payload._id }, payload)
      res.json(apply)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getApplys (req, res, next) {
    try {
      const applys = await Apply.find({})
      res.json(applys)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getApply (req, res, next) {
    const { id } = req.params
    try {
      const apply = await Apply.findById(id)
      res.json(apply)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}

module.exports = applyController
