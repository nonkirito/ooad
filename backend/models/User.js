const mongoose = require('mongoose')
const Schema = mongoose.Schema
const userSchema = new Schema({
  name: String,
  username: String,
  password: String,
  confirmpassword: String,
  type: String
})

module.exports = mongoose.model('User', userSchema)
