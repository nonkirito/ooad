const Position = require('../models/Position')
const positionController = {
  positionList: [
    {
      id: 1,
      name: 'นักออกแบบแฟชั่น',
      type: 'งานออกแบบ'
    },
    {
      id: 2,
      name: 'นักออกแบบกราฟฟิค',
      type: 'งานออกแบบ'
    },
    {
      id: 3,
      name: 'โปรแกรมเมอร์',
      type: 'งานไอที'
    }
  ],
  lastId: 4,
  async addPosition (req, res, next) {
    const payload = req.body
    console.log(payload)
    const position = new Position(payload)
    try {
      const newposition = await position.save()
      res.json(newposition)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async updatePosition (req, res, next) {
    const payload = req.body
    try {
      console.log(payload)
      const position = await Position.updateOne({ _id: payload._id }, payload)
      res.json(position)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async deletePosition (req, res, next) {
    const { id } = req.params
    try {
      const position = await Position.deleteOne({ _id: id })
      res.json(position)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getPositions (req, res, next) {
    try {
      const positions = await Position.find({})
      res.json(positions)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getPosition (req, res, next) {
    const { id } = req.params
    try {
      const position = await Position.findById(id)
      res.json(position)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}

module.exports = positionController
