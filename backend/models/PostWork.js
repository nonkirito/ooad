const mongoose = require('mongoose')
const Schema = mongoose.Schema
const postSchema = new Schema({
  position: String,
  salary: Number,
  workplace: String,
  province: String,
  numberofapplicants: Number,
  condition: String,
  welfare: String,
  opendate: Date,
  closedate: Date,
  operation: String
})

module.exports = mongoose.model('PostWork', postSchema)
