const express = require('express')
const router = express.Router()
const positionsController = require('../controller/PositionsController')

/* GET positions listing. */
router.get('/', positionsController.getPositions)

router.get('/:id', positionsController.getPosition)

router.post('/', positionsController.addPosition)

router.put('/', positionsController.updatePosition)

router.delete('/:id', positionsController.deletePosition)

module.exports = router
