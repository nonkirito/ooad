const express = require('express')
const router = express.Router()
const resumeController = require('../controller/ResumeController')
// const Resume = require('../models/Resume')
/* GET users listing. */

router.get('/', resumeController.getResume)

router.get('/:id', resumeController.getResumeOne)

router.post('/', resumeController.addResume)

router.put('/', resumeController.updateResume)

router.delete('/:id', resumeController.deleteResume)

module.exports = router
