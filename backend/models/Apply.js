const mongoose = require('mongoose')
const Schema = mongoose.Schema
const applySchema = new Schema({
  name: String,
  surname: String,
  tel: String,
  email: String,
  resumeId: String,
  experience: Number,
  expectedSalary: Number,
  status: String,
  interviewDate: Date
})

module.exports = mongoose.model('Applys', applySchema)
