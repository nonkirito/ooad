import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/users',
    name: 'users',
    component: () => import('../views/Users')
  },
  {
    path: '/positions',
    name: 'Positions',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/Positions/')
  },
  {
    path: '/resume',
    name: 'Resume',
    component: () => import('../views/Resume/')
  },
  {
    path: '/postwork',
    name: 'Postwork',
    component: () => import('../views/Postwork/')
  },
  {
    path: '/register',
    name: 'register',
    component: () => import('../views/register.vue')
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import('../views/login.vue')
  },
  {
    path: '/member',
    name: 'Member',
    meta: {
      requiresAuth: true
    },
    component: () => import('../views/Member.vue')
  },
  {
    path: '/company',
    name: 'Company',
    component: () => import('../views/Company/company.vue')
  },
  {
    path: '/detail',
    name: 'Detail',
    component: () => import('../views/PostDetail/index.vue')
  },
  {
    path: '/companyhome',
    name: 'CompanyHome',
    meta: {
      requiresAuth: true
    },
    component: () => import('../views/CompanyHome/companyHome.vue')
  },
  {
    path: '/apply',
    name: 'Apply',
    component: () => import('../views/ApplyJobs/index.vue')
  },
  {
    path: '/registerCompany',
    name: 'registerCompany',
    component: () => import('../views/registerCompany.vue')
  },
  {
    path: '/loginCompany',
    name: 'loginCompany',
    component: () => import('../views/loginCompany.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})
router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (localStorage.getItem('jwt') == null) {
      next({
        path: '/login'
      })
    } else {
      next()
    }
  } else {
    next()
  }
})
router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (localStorage.getItem('jwt') == null) {
      next({
        path: '/loginCompany'
      })
    } else {
      next()
    }
  } else {
    next()
  }
})

export default router
